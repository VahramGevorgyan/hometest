//
//  HomeViewController.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import UIKit

extension HomeViewController {
    func hideOnOutsideTouchKeyboard() {
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}


class HomeViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var messageTextArea: UITextView!
    @IBOutlet private weak var indicator: UIActivityIndicatorView!
    
    
    /**
     Intro for home test.
     
     MessageParserHandler responsible for parsing final message. By default all 3 parsers are used MessageParserFactory.defaultParser().
     As MessageParsers are loosely coupled, and encapsulated, we can always add new parsers, remove exitsing one, without refactoring or breaking functionality.
     All we need is to change in MessageParserFactory , and curresponding parser need to implement MessageParser. The way how parsers designed and arhitectured, it's like playing with legos, we can very easily remove/add parsers.
     
     Thank you, Vahram
     */
    private let parserHandler = MessageParserHandler()
    
    private var message = "" {
        didSet {
            self.updateMessage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.delegate = self
        self.hideOnOutsideTouchKeyboard()
    }
    
    @IBAction func parseMessage(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.textField.text = "http://www.nbcolympics.com @Anna (cool)"
        case 2:
            self.textField.text = "Hi my name is @Vahram (smile) "
            
        default:
            break;
        }
        
        self.message = textField.text ?? ""
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.message = textField.text ?? ""
        return true
    }
    
    private func updateMessage() {
        self.indicator.startAnimating()
        parserHandler.parseFinalMessage(message: self.message) { result in
            self.messageTextArea.text = result
            self.indicator.stopAnimating()
            self.textField.resignFirstResponder()
            print("Final result \(result)")
        }
    }
    
}

