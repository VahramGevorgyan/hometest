//
//  MessageMentionsParser.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import Foundation

class MessageMentionsParser: MessageParser {
    func parseMessage(message: String, result: ([Any]) -> Void) {
        let regexForMentions = "\\B@\\w\\S*\\b"
        result(message.match(regex: regexForMentions))
    }
    
    func jsonFieldName() -> String {
        return "mentions"
    }
}
