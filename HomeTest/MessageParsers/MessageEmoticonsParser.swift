//
//  MessageEmoticonsParser.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import Foundation

class MessageEmoticonsParser: MessageParser {
    func parseMessage(message: String, result: ([Any]) -> Void) {
        let regexForEmoticons = "\\(\\w{1,15}\\)"
        result(message.match(regex: regexForEmoticons))
    }
    
    func jsonFieldName() -> String {
        return "emoticons"
    }
}
