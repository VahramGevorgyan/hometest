//
//  ParserHandler.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import Foundation

class MessageParserHandler {
    
    func parseFinalMessage(withMessageParsers parsers:[MessageParser] = MessageParserFactory.defaultParser(), message: String, completition:@escaping (String) -> Void) {
        
        let dispatchGroup = DispatchGroup()
        let params:NSMutableDictionary = NSMutableDictionary()
        
        DispatchQueue.global().async {
            for parser in parsers {
                dispatchGroup.enter()
                parser.parseMessage(message: message, result: { result in
                    params.setValue(result, forKey: parser.jsonFieldName())
                    dispatchGroup.leave()
                })
            }
            
            dispatchGroup.notify(queue: DispatchQueue.main) {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let jsonString = String(data: jsonData, encoding: .utf8)?.replacingOccurrences(of: "\\", with: "")
                    completition(jsonString ?? "")
                }
                    
                catch {
                    completition("")
                }
                
            }
        }
    }
}

