//
//  MessageParser.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import Foundation

protocol MessageParser {
    func parseMessage(message: String, result: ([Any]) -> Void)
    func jsonFieldName() -> String
}
