//
//  MessageURLParser.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import Foundation

struct MessageURLItem {
    var url: String
    var title: String
    
    func toJSON() -> [String: String] {
        return ["url": url, "title": title]
    }
}

class MessageURLParser: MessageParser {
    func parseMessage(message: String, result: ([Any]) -> Void) {
        let regexForUrl = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urls = message.match(regex: regexForUrl)
        
        var urlItems = [[String: Any]]()
        
        for url in urls {
            guard let finalURL = URL(string: url) else { continue }
            let title = finalURL.parseTitleFromUrl()
            let urlMessageItem = MessageURLItem(url: url, title: title)
            urlItems.append(urlMessageItem.toJSON())
        }
        result(urlItems)
    }
    
    func jsonFieldName() -> String {
        return "links"
    }
}
