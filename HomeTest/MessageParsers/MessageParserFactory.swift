//
//  MessageParserFactory.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import Foundation


enum MessageParserType {
    case mentions, emoticons, urls
}

class MessageParserFactory {
    class func parserWithType(type: MessageParserType) -> MessageParser {
        var parser: MessageParser
        
        switch type {
        case .emoticons:
            parser = MessageEmoticonsParser()
        case .mentions:
            parser = MessageMentionsParser()
        case .urls:
            parser = MessageURLParser()
        }
        
        return parser;
    }
    
    class func defaultParser() -> [MessageParser] {
        return [MessageEmoticonsParser(), MessageMentionsParser(), MessageURLParser()]
    }
    
    class func defaultParserWithoutURL() -> [MessageParser] {
        return [MessageEmoticonsParser(), MessageMentionsParser()]
    }
}
