//
//  URL+Extension.swift
//  HomeTest
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import Foundation
extension URL {
    func parseTitleFromUrl() -> String {
        var title = ""
        do {
            let stringURL = try String(contentsOf: self)
            title = stringURL.slice(from: "<title>", to: "</title") ?? ""
        }
        catch {
            title = "unknown"
        }
        return title
    }
}
