//
//  HomeTestTests.swift
//  HomeTestTests
//
//  Created by Vahram Gevorgyan on 12/1/16.
//  Copyright © 2016 Vahram Gevorgyan. All rights reserved.
//

import XCTest
@testable import HomeTest

class HomeTestTests: XCTestCase {
    
    func testMentionsParser() {
        let mentionParser = MessageParserFactory.parserWithType(type: .mentions)
        mentionParser.parseMessage(message: "Hi this is unit test, @Vahram") { result in
            XCTAssertTrue(result.count == 1 && result.first! as! String == "@Vahram")
        }
    }
    
    func testURLParser() {
        self.measure {
            let urlParser = MessageParserFactory.parserWithType(type: .urls)
            urlParser.parseMessage(message: "https://google.com") { result in
                print(result.first!)
                XCTAssertTrue(result.count == 1 && (result.first! as! Dictionary)["title"] == "Google")
            }
        }
    }
    
    func testEmoticonsParser() {
        let emoticonParser = MessageParserFactory.parserWithType(type: .emoticons)
        emoticonParser.parseMessage(message: "(smile) (tooooooolooooooooonngggg") { result in
            XCTAssertTrue(result.count == 1 && result.first! as! String == "(smile)")
        }
    }
    
}
