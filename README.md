# README #


### Home Test source code ###

* iOS app with message parsing functionality
* Well designed architecture (design patterns, mvc, loosely coupled design)
* Very easy to add new kind of message  parsers in future
* Self-explanatory code
* Written in Swift 3
* Include Unit Tests